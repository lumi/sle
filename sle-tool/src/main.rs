use {
    std::{
        io::{
            prelude::*,
            self,
        },
        path::{
            PathBuf,
        },
        process::{
            Command,
            Child,
            Stdio,
        },
    },
    structopt::StructOpt,
    sle::{
        Encodable,
        Value,
    },
};

mod ansi_encoder;
mod tester;

#[derive(StructOpt)]
struct Opt {
    #[structopt(subcommand)]
    cmd: Cmd,
}

#[derive(StructOpt)]
enum Cmd {
    /// Take SLE from stdin and print it to stdout, in a pretty format.
    #[structopt(name = "prettify")]
    Prettify {
        /// Don't color using ANSI codes.
        #[structopt(short = "c", long = "no-color")]
        no_color: bool,
    },
    /// Take SLE from stdin and print it to stdout, in compact format.
    #[structopt(name = "compactify")]
    Compactify,
    /// Run the tester on an executable.
    #[structopt(name = "test")]
    Test {
        #[structopt(name = "executable")]
        executable: PathBuf,
    },
}

type BoxError = Box<dyn std::error::Error>;

fn main() -> Result<(), BoxError> {
    let opt = Opt::from_args();
    match opt.cmd {
        Cmd::Prettify { no_color } => {
            let mut stdin = io::stdin();
            let mut buf = String::new();
            stdin.read_to_string(&mut buf)?;
            let val: Value = sle::decode(&buf)?;
            buf.clear();
            if no_color {
                sle::encode_pretty(&val, &mut buf);
                println!("{}", buf);
            }
            else {
                let mut stdout = io::stdout();
                let mut encoder = ansi_encoder::AnsiEncoder::new(&mut stdout);
                val.encode(&mut encoder)?;
            }
        },
        Cmd::Compactify => {
            let mut stdin = io::stdin();
            let mut buf = String::new();
            stdin.read_to_string(&mut buf)?;
            let val: Value = sle::decode(&buf)?;
            buf.clear();
            sle::encode(&val, &mut buf);
            println!("{}", buf);
        },
        Cmd::Test { executable } => {
            let mut child = Command::new(executable)
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()
                .unwrap();
            let succeeded = tester::test(child.stdin.as_mut().unwrap(), child.stdout.as_mut().unwrap())?;
            if succeeded {
                println!("test succeeded");
            }
            else {
                println!("test failed");
            }
        },
    }
    Ok(())
}
