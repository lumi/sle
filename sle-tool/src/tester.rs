use {
    std::{
        io::{
            Read,
            Write,
            BufRead,
            BufReader,
            Result as IoResult,
        },
    },
    sle::{
        Encodable,
        Decodable,
        Value,
    },
};

#[derive(Debug, Encodable)]
pub enum Challenge {
    Get {
        path: String,
    },
    Set {
        path: String,
        value: Value,
    },
}

#[derive(Debug, PartialEq, Decodable)]
pub enum Response {
    Success {
        result: Value,
    },
    Error {
        reason: String,
    },
}

pub struct Tester<R: Read, W: Write> {
    reader: BufReader<R>,
    writer: W,
}

impl<R: Read, W: Write> Tester<R, W> {
    pub fn challenge(&mut self, challenge: &Challenge, expect: &Response) -> IoResult<bool> {
        let mut buf = String::new();
        sle::encode(challenge, &mut buf);
        println!("CHALLENGE {}", buf);
        buf.push('\n');
        self.writer.write_all(buf.as_bytes())?;
        buf.clear();
        self.reader.read_line(&mut buf)?;
        let response: Response = match sle::decode(&buf) {
            Ok(res) => res,
            Err(_) => {
                return Ok(false);
            },
        };
        Ok(&response == expect)
    }
}

pub fn test<R: Read, W: Write>(writer: W, reader: R) -> IoResult<bool> {
    let mut tester = Tester { reader: BufReader::new(reader), writer };
    tester.challenge(&Challenge::Set {
        path: "/".to_owned(),
        value: Value::Nil,
    }, &Response::Success { result: Value::Nil })?;
    Ok(true)
}
