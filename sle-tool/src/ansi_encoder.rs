use {
    std::{
        io::{
            self,
            Write,
        },
    },
    sle::{
        Encodable,
        enc::{
            Encoder,
            SequenceEncoder,
            RecordEncoder,
        },
    },
    ansi_term::{
        ANSIString,
        ANSIStrings,
        Colour,
    },
};

pub struct AnsiEncoder<W: Write> {
    out: W,
    indent: usize,
    col: usize,
}

impl<W: Write> AnsiEncoder<W> {
    pub fn new(out: W) -> AnsiEncoder<W> {
        AnsiEncoder {
            out,
            indent: 0,
            col: 0,
        }
    }

    pub fn push(&mut self, text: &str) -> Result<(), io::Error> {
        self.col += text.chars().count();
        self.out.write_all(text.as_bytes())
    }

    pub fn push_ansi(&mut self, text: ANSIString) -> Result<(), io::Error> {
        self.col += text.chars().count();
        write!(self.out, "{}", text)
    }

    pub fn push_ansi_multi(&mut self, text: ANSIStrings) -> Result<(), io::Error> {
        let cols: usize = text.0.into_iter().map(|s| s.chars().count()).sum();
        self.col += cols;
        write!(self.out, "{}", text)
    }

    pub fn set_indent(&mut self, indent: usize) -> usize {
        let prev_indent = self.indent;
        self.indent = indent;
        prev_indent
    }

    pub fn newline(&mut self) -> Result<(), io::Error> {
        self.col = self.indent;
        self.out.write_all(b"\n")?;
        for _ in 0..self.indent {
            self.out.write_all(b" ")?;
        }
        Ok(())
    }

    pub fn col(&self) -> usize {
        self.col
    }
}

impl<'a, W: Write> Encoder for &'a mut AnsiEncoder<W> {
    type Error = io::Error;
    type SequenceEncoder = AnsiSequenceEncoder<'a, W>;
    type RecordEncoder = AnsiRecordEncoder<'a, W>;

    fn encode_nil(self) -> Result<(), io::Error> {
        self.push_ansi(Colour::Red.paint("#n"))?;
        Ok(())
    }

    fn encode_real(self, real: f64) -> Result<(), io::Error> {
        let mut s = real.to_string();
        if real.floor() == real {
            s.push('.');
        }
        self.push_ansi(Colour::Red.paint(s))
    }

    fn encode_integer(self, int: i64) -> Result<(), io::Error> {
        self.push_ansi(Colour::Red.paint(int.to_string()))?;
        Ok(())
    }

    fn encode_bool(self, b: bool) -> Result<(), io::Error> {
        if b {
            self.push_ansi(Colour::Red.paint("#t"))?;
        }
        else {
            self.push_ansi(Colour::Red.paint("#f"))?;
        }
        Ok(())
    }

    fn encode_text(self, text: &str) -> Result<(), io::Error> {
        self.push_ansi_multi(ANSIStrings(&[
            Colour::Green.paint("\""),
            Colour::Green.paint(text.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\"")),
            Colour::Green.paint("\""),
        ]))
    }

    fn encode_atom(self, atom: &str) -> Result<(), io::Error> {
        self.push_ansi_multi(ANSIStrings(&[
            Colour::Red.paint(":"),
            Colour::Red.paint(atom),
        ]))?;
        Ok(())
    }

    fn encode_sequence(self) -> Result<AnsiSequenceEncoder<'a, W>, io::Error> {
        self.push_ansi(Colour::Yellow.paint("["))?;
        let prev_indent = self.set_indent(self.col() + 1);
        Ok(AnsiSequenceEncoder {
            enc: self,
            first: true,
            prev_indent,
        })
    }

    fn encode_record(self, name: &str) -> Result<AnsiRecordEncoder<'a, W>, io::Error> {
        self.push_ansi(Colour::Yellow.paint("("))?;
        self.push_ansi(Colour::Blue.paint(name))?;
        let prev_indent = self.set_indent(self.col() + 1);
        Ok(AnsiRecordEncoder {
            enc: self,
            first: true,
            prev_indent,
        })
    }
}

pub struct AnsiSequenceEncoder<'a, W: Write> {
    enc: &'a mut AnsiEncoder<W>,
    first: bool,
    prev_indent: usize,
}

impl<'a, W: Write> SequenceEncoder for AnsiSequenceEncoder<'a, W> {
    type Error = io::Error;

    fn encode_item<E: Encodable>(&mut self, item: &E) -> Result<(), io::Error> {
        if self.first {
            self.enc.push(" ")?;
            self.first = false;
        }
        else {
            self.enc.newline()?;
        }
        item.encode(&mut *self.enc)
    }

    fn end(self) -> Result<(), io::Error> {
        if !self.first {
            self.enc.push(" ")?;
        }
        self.enc.push_ansi(Colour::Yellow.paint("]"))?;
        self.enc.set_indent(self.prev_indent);
        Ok(())
    }
}

pub struct AnsiRecordEncoder<'a, W: Write> {
    enc: &'a mut AnsiEncoder<W>,
    first: bool,
    prev_indent: usize,
}

impl<'a, W: Write> RecordEncoder for AnsiRecordEncoder<'a, W> {
    type Error = io::Error;

    fn encode_field<E: Encodable>(&mut self, field: &str, value: &E) -> Result<(), io::Error> {
        if self.first {
            self.enc.push(" ")?;
            self.first = false;
        }
        else {
            self.enc.newline()?;
        }
        self.enc.push_ansi(Colour::Yellow.paint(field))?;
        self.enc.push(" ")?;
        value.encode(&mut *self.enc)
    }

    fn end(self) -> Result<(), io::Error> {
        self.enc.push_ansi(Colour::Yellow.paint(")"))?;
        self.enc.set_indent(self.prev_indent);
        Ok(())
    }
}
