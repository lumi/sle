# Tokens

```
whitespace = [ \n]

bare = [a-z][a-z0-9/-\.]*
bool = '#t' | '#f'
integer = -?[0-9]+
real = -?[0-9]+\.[0-9]+
text = "([^"\]|\n|\"|\\)*"
atom = :[a-z0-9/-\.]+

open-record = '('
open-sequence = '['

close-record = ')'
close-sequence = ']'

nil : '#n'
```

# Data types

## Primitive

### Definition

```
prim = integer | real | bool | text | atom | nil
```

### Examples

```
3
4.5
"hello"
:hello
#t
#f
#n
```

## Composite

### Record

#### Definition

```
record = open-record bare (bare prim)* close-record
```

#### Example

```
(record-name
  key1 val1
  key2 val2
  key3 val3)
```

### Sequence

#### Definition

```
sequence = open-sequence prim* close-sequence
```

#### Example

```
[item1 item2 item3]
```
