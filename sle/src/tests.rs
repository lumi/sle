use {
    proptest::{
        prelude::*,
    },
};

use crate::{
    Value,
    Record,
    encode,
    decode,
};

fn arb_record(field_strat: impl Strategy<Value = Value>) -> impl Strategy<Value = Record> {
    let name_strat = "[a-z][a-z0-9/\\.-]*";
    let fields_strat = prop::collection::vec((name_strat, field_strat), 0..10);
    (name_strat, fields_strat).prop_map(|(n, f)| {
        let mut rec = Record::new(n);
        for (field, value) in f {
            rec.insert(field, value);
        }
        rec
    })
}

fn arb_value() -> impl Strategy<Value = Value> {
    let leaf = prop_oneof! [
        Just(Value::Nil),
        any::<bool>().prop_map(Value::Bool),
        any::<i64>().prop_map(Value::Integer),
        any::<f64>().prop_map(Value::Real),
        ".*".prop_map(Value::Text),
        "[a-z0-9/\\.-]+".prop_map(Value::Atom),
    ];
    leaf.prop_recursive(
        8,
        256,
        16,
        |inner| prop_oneof! [
            prop::collection::vec(inner.clone(), 0..10).prop_map(Value::Sequence),
            arb_record(inner).prop_map(Value::Record),
        ],
    )
}

proptest! {
    #![proptest_config(ProptestConfig { cases: 1000, .. ProptestConfig::default() })]
    #[test]
    fn prop_encode_decode(v in arb_value()) {
        let mut out = String::new();
        encode(&v, &mut out);
        let v_new: Value = decode(&out).unwrap();
        assert_eq!(v, v_new, "encoded output: {}", out);
    }
}
