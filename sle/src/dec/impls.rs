use {
    std::{
        marker::PhantomData,
    },
};

use super::{
    Visitor,
    DecoderError,
    Decodable,
    Decoder,
    UntypedItems,
};

pub struct UnitVisitor;

impl<'d> Visitor<'d> for UnitVisitor {
    type Value = ();

    fn visit_nil(self) -> Result<(), DecoderError> {
        Ok(())
    }
}

pub struct BoolVisitor;

impl<'d> Visitor<'d> for BoolVisitor {
    type Value = bool;

    fn visit_bool(self, b: bool) -> Result<bool, DecoderError> {
        Ok(b)
    }
}

pub struct I64Visitor;

impl<'d> Visitor<'d> for I64Visitor {
    type Value = i64;

    fn visit_integer(self, int: i64) -> Result<i64, DecoderError> {
        Ok(int)
    }
}

pub struct U64Visitor;

impl<'d> Visitor<'d> for U64Visitor {
    type Value = u64;

    fn visit_integer(self, int: i64) -> Result<u64, DecoderError> {
        Ok(int as u64)
    }
}

pub struct F32Visitor;

impl<'d> Visitor<'d> for F32Visitor {
    type Value = f32;

    fn visit_real(self, real: f64) -> Result<f32, DecoderError> {
        Ok(real as f32)
    }
}

pub struct F64Visitor;

impl<'d> Visitor<'d> for F64Visitor {
    type Value = f64;

    fn visit_real(self, real: f64) -> Result<f64, DecoderError> {
        Ok(real)
    }
}

pub struct StringVisitor;

impl<'d> Visitor<'d> for StringVisitor {
    type Value = String;

    fn visit_text(self, text: &str) -> Result<String, DecoderError> {
        Ok(text.to_owned())
    }
}

pub struct VecVisitor<T: Decodable> {
    _marker: PhantomData<T>,
}

impl<'d, T: Decodable> Visitor<'d> for VecVisitor<T> {
    type Value = Vec<T>;

    fn visit_sequence<'a>(self, items: UntypedItems<'d, 'a>) -> Result<Vec<T>, DecoderError> {
        let mut typed = items.select::<T>();
        let mut ret = Vec::new();
        while let Some(item) = typed.next_item()? {
            ret.push(item);
        }
        Ok(ret)
    }
}

impl Decodable for () {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<(), DecoderError> {
        dec.decode_nil(UnitVisitor)
    }
}

impl Decodable for bool {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<bool, DecoderError> {
        dec.decode_bool(BoolVisitor)
    }
}

impl Decodable for i64 {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<i64, DecoderError> {
        dec.decode_integer(I64Visitor)
    }
}

impl Decodable for u64 {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<u64, DecoderError> {
        dec.decode_integer(U64Visitor)
    }
}

impl Decodable for f32 {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<f32, DecoderError> {
        dec.decode_real(F32Visitor)
    }
}

impl Decodable for f64 {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<f64, DecoderError> {
        dec.decode_real(F64Visitor)
    }
}

impl Decodable for String {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<String, DecoderError> {
        dec.decode_text(StringVisitor)
    }
}

impl<T: Decodable> Decodable for Vec<T> {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<Vec<T>, DecoderError> {
        dec.decode_sequence(VecVisitor { _marker: PhantomData })
    }
}

impl<T: Decodable> Decodable for Option<T> {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<Option<T>, DecoderError> {
        dec.decode_option::<T>()
    }

    fn extract(_field: &'static str, opt: Option<Option<T>>) -> Result<Option<T>, DecoderError> {
        Ok(opt.unwrap_or(None))
    }
}

impl<T: Decodable> Decodable for Box<T> {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<Box<T>, DecoderError> {
        let inner = T::decode(dec)?;
        Ok(Box::new(inner))
    }
}
