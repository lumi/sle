use {
    logos::{
        Logos,
        Lexer,
    },
    std::borrow::Cow,
};

use super::{
    error::DecoderError,
};

/// A kind of token.
#[derive(Debug, Clone, Copy, Logos, PartialEq)]
#[logos(trivia = "[ \n]")]
pub enum TokenType {
    #[end]
    End,

    #[error]
    Error,

    #[regex = "#[ft]"]
    Bool,

    #[regex = "-?[0-9]+"]
    Integer,

    #[regex = "-?[0-9]+\\.[0-9]*"]
    Real,

    #[regex = "[a-z][a-z0-9/\\.-]*"]
    Bare,

    #[regex = ":[a-z0-9/\\.-]+"]
    Atom,

    #[regex = r#""([^"\\]|\\n|\\"|\\\\)*""#]
    Text,

    #[token = "("]
    OpenRecord,

    #[token = ")"]
    CloseRecord,

    #[token = "["]
    OpenSequence,

    #[token = "]"]
    CloseSequence,

    #[token = "#n"]
    Nil,
}

/// A token.
#[derive(Debug, Clone, PartialEq)]
pub enum Token<'t> {
    Bool(bool),
    Integer(i64),
    Real(f64),
    Bare(&'t str),
    Atom(&'t str),
    Text(&'t str),
    OpenRecord,
    CloseRecord,
    OpenSequence,
    CloseSequence,
    Nil,
}

impl<'t> Token<'t> {
    /// Get the type of a token.
    pub fn ty(&self) -> TokenType {
        match *self {
            Token::Bool(_) => TokenType::Bool,
            Token::Integer(_) => TokenType::Integer,
            Token::Real(_) => TokenType::Real,
            Token::Bare(_) => TokenType::Bare,
            Token::Atom(_) => TokenType::Atom,
            Token::Text(_) => TokenType::Text,
            Token::OpenRecord => TokenType::OpenRecord,
            Token::CloseRecord => TokenType::CloseRecord,
            Token::OpenSequence => TokenType::OpenSequence,
            Token::CloseSequence => TokenType::CloseSequence,
            Token::Nil => TokenType::Nil,
        }
    }

    /// Unwrap this as a nil token, or return an error.
    pub fn expect_nil(self) -> Result<(), DecoderError> {
        if let Token::Nil = self {
            Ok(())
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Nil]))
        }
    }

    /// Unwrap this as a bool token, or return an error.
    pub fn expect_bool(self) -> Result<bool, DecoderError> {
        if let Token::Bool(b) = self {
            Ok(b)
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Bool]))
        }
    }

    /// Unwrap this as an integer token, or return an error.
    pub fn expect_integer(self) -> Result<i64, DecoderError> {
        if let Token::Integer(int) = self {
            Ok(int)
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Integer]))
        }
    }

    /// Unwrap this as a real token, or return an error.
    pub fn expect_real(self) -> Result<f64, DecoderError> {
        if let Token::Real(real) = self {
            Ok(real)
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Real]))
        }
    }

    /// Unwrap this as bare token, or return an error.
    pub fn expect_bare(self) -> Result<&'t str, DecoderError> {
        if let Token::Bare(bare) = self {
            Ok(bare)
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Bare]))
        }
    }

    /// Unwrap this as an atom token, or return an error.
    pub fn expect_atom(self) -> Result<&'t str, DecoderError> {
        if let Token::Atom(atom) = self {
            Ok(atom)
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Atom]))
        }
    }

    /// Unwrap this as a text token, or return an error.
    pub fn expect_text(self) -> Result<Cow<'t, str>, DecoderError> {
        if let Token::Text(text) = self {
            Ok(process_escapes(text))
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::Text]))
        }
    }

    /// Unwrap this as an open-sequence token, or return an error.
    pub fn expect_open_sequence(self) -> Result<(), DecoderError> {
        if let Token::OpenSequence = self {
            Ok(())
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::OpenSequence]))
        }
    }

    /// Unwrap this as an close-sequence token, or return an error.
    pub fn expect_close_sequence(self) -> Result<(), DecoderError> {
        if let Token::CloseSequence = self {
            Ok(())
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::CloseSequence]))
        }
    }

    /// Unwrap this as an open-record token, or return an error.
    pub fn expect_open_record(self) -> Result<(), DecoderError> {
        if let Token::OpenRecord = self {
            Ok(())
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::OpenRecord]))
        }
    }

    /// Unwrap this as an close-record token, or return an error.
    pub fn expect_close_record(self) -> Result<(), DecoderError> {
        if let Token::CloseRecord = self {
            Ok(())
        }
        else {
            Err(DecoderError::UnexpectedToken(self.ty(), &[TokenType::CloseRecord]))
        }
    }
}

fn process_escapes(s: &str) -> Cow<str> {
    if s.chars().any(|c| c == '\\') {
        let mut out = String::with_capacity(s.len());
        let mut in_escape = false;
        for c in s.chars() {
            if in_escape {
                match c {
                    '\\' => out.push('\\'),
                    '"' => out.push('"'),
                    'n' => out.push('\n'),
                    c => out.push(c),
                }
                in_escape = false;
            }
            else if c == '\\' {
                in_escape = true;
            }
            else {
                out.push(c);
            }
        }
        Cow::Owned(out)
    }
    else {
        Cow::Borrowed(s)
    }
}

fn convert_token<'t>(lex: &Lexer<TokenType, &'t str>) -> Result<Token<'t>, DecoderError> {
    Ok(match lex.token {
        TokenType::End => Err(DecoderError::EndOfStream)?,
        TokenType::Error => Err(DecoderError::InvalidToken(lex.slice().to_owned()))?,
        TokenType::Bool => Token::Bool(lex.slice() == "#t"),
        TokenType::Integer => Token::Integer(lex.slice().parse().unwrap()),
        TokenType::Real => Token::Real(lex.slice().parse().unwrap()),
        TokenType::Bare => Token::Bare(lex.slice()),
        TokenType::Atom => Token::Atom(&lex.slice()[1..]),
        TokenType::Text => {
            let len = lex.slice().len();
            let inner = &lex.slice()[1..len - 1];
            Token::Text(inner)
        },
        TokenType::Nil => Token::Nil,
        TokenType::OpenRecord => Token::OpenRecord,
        TokenType::CloseRecord => Token::CloseRecord,
        TokenType::OpenSequence => Token::OpenSequence,
        TokenType::CloseSequence => Token::CloseSequence,
    })
}

/// A tokenizer.
pub struct Tokenizer<'t> {
    lex: Lexer<TokenType, &'t str>,
}

impl<'t> Tokenizer<'t> {
    /// Create a tokenizer from a string slice.
    pub fn from_str(text: &'t str) -> Tokenizer<'t> {
        Tokenizer {
            lex: TokenType::lexer(text),
        }
    }

    /// Get the next token.
    pub fn next(&mut self) -> Result<Token, DecoderError> {
        let tok = convert_token(&self.lex)?;
        self.lex.advance();
        Ok(tok)
    }

    /// Peek at the next token.
    pub fn peek(&mut self) -> Result<Token, DecoderError> {
        convert_token(&self.lex)
    }

    /// Peek at the token after the next token.
    pub fn peek2(&mut self) -> Result<Token, DecoderError> {
        let mut lex = self.lex.clone();
        lex.advance();
        convert_token(&lex)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_escapes() {
        assert_eq!(process_escapes("abcd"), "abcd");
        assert_eq!(process_escapes("\\\"abcd\\\""), "\"abcd\"");
        assert_eq!(process_escapes("\\\"ab\\ncd\\\""), "\"ab\ncd\"");
    }
}
