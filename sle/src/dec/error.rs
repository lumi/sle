use {
    std::fmt,
};

use super::{
    token::TokenType,
};

#[derive(Debug)]
pub enum DecoderError {
    InvalidToken(String),
    UnexpectedToken(TokenType, &'static [TokenType]),
    UnexpectedType(&'static str),
    EndOfStream,
    MissingField(&'static str),
    UnexpectedField(String),
    UnexpectedRecordName(String, &'static [&'static str]),
    UnexpectedAtom(String, &'static [&'static str]),
    DuplicateKey,
    DuplicateField(&'static str),
}

impl std::error::Error for DecoderError {}

impl fmt::Display for DecoderError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DecoderError::InvalidToken(ref s) => write!(f, "invalid token {:?}", s),
            DecoderError::UnexpectedToken(tok, expected) => write!(f, "unexpected token {:?}, expected one of {:?}", tok, expected),
            DecoderError::UnexpectedType(type_) => write!(f, "unexpected type {:?}", type_),
            DecoderError::EndOfStream => write!(f, "the end of the stream has been reached"),
            DecoderError::MissingField(name) => write!(f, "field {:?} missing", name),
            DecoderError::UnexpectedField(ref name) => write!(f, "unexpected field {:?}", name),
            DecoderError::UnexpectedRecordName(ref name, expected) => write!(f, "unexpected record name {:?}, expected one of {:?}", name, expected),
            DecoderError::UnexpectedAtom(ref atom, expected) => write!(f, "unexpected atom {:?}, expected one of {:?}", atom, expected),
            DecoderError::DuplicateKey => write!(f, "duplicate key"),
            DecoderError::DuplicateField(name) => write!(f, "duplicate field {:?}", name),
        }
    }
}
