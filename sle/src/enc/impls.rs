use super::{
    Encodable,
    Encoder,
    SequenceEncoder,
};

impl Encodable for () {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_nil()
    }
}

impl Encodable for bool {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_bool(*self)
    }
}

impl Encodable for String {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_text(self)
    }
}

impl Encodable for &str {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_text(self)
    }
}

impl Encodable for u64 {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_integer(*self as i64)
    }
}

impl Encodable for i64 {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_integer(*self)
    }
}

impl Encodable for f64 {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_real(*self)
    }
}

impl Encodable for f32 {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        enc.encode_real(*self as f64)
    }
}

impl<T: Encodable> Encodable for Option<T> {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        match *self {
            Some(ref inner) => inner.encode(enc),
            None => enc.encode_nil(),
        }
    }
}

impl<T: Encodable> Encodable for Vec<T> {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        let mut seq = enc.encode_sequence()?;
        for item in self {
            seq.encode_item(item)?;
        }
        seq.end()
    }
}

impl<'a, T: Encodable> Encodable for &'a T {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        (*self).encode(enc)
    }
}

impl<'a, T: Encodable> Encodable for &'a mut T {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        (**self).encode(enc)
    }
}

impl<'a, T: Encodable> Encodable for Box<T> {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        (**self).encode(enc)
    }
}
