use {
    core::convert::Infallible,
};

use super::{
    EncoderInner,
    Encoder,
    Encodable,
    SequenceEncoder,
    RecordEncoder,
};

pub struct PrettyEncoder<'e> {
    inner: EncoderInner<'e>,
}

impl<'e> PrettyEncoder<'e> {
    pub fn new(out: &'e mut String) -> PrettyEncoder<'e> {
        PrettyEncoder {
            inner: EncoderInner::new(out),
        }
    }
}

impl<'e, 'a> Encoder for &'a mut PrettyEncoder<'e> {
    type Error = Infallible;
    type SequenceEncoder = PrettySequenceEncoder<'e, 'a>;
    type RecordEncoder = PrettyRecordEncoder<'e, 'a>;

    fn encode_nil(self) -> Result<(), Infallible> {
        self.inner.push("#n");
        Ok(())
    }

    fn encode_real(self, real: f64) -> Result<(), Infallible> {
        self.inner.push(&real.to_string());
        if real.floor() == real {
            self.inner.push(".");
        }
        Ok(())
    }

    fn encode_integer(self, int: i64) -> Result<(), Infallible> {
        self.inner.push(&int.to_string());
        Ok(())
    }

    fn encode_bool(self, b: bool) -> Result<(), Infallible> {
        if b {
            self.inner.push("#t");
        }
        else {
            self.inner.push("#f");
        }
        Ok(())
    }

    fn encode_text(self, text: &str) -> Result<(), Infallible> {
        self.inner.push("\"");
        self.inner.push(&text.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\""));
        self.inner.push("\"");
        Ok(())
    }

    fn encode_atom(self, atom: &str) -> Result<(), Infallible> {
        self.inner.push(":");
        self.inner.push(atom);
        Ok(())
    }

    fn encode_sequence(self) -> Result<PrettySequenceEncoder<'e, 'a>, Infallible> {
        self.inner.push("[");
        let prev_indent = self.inner.set_indent(self.inner.col() + 1);
        Ok(PrettySequenceEncoder {
            enc: self,
            first: true,
            prev_indent,
        })
    }

    fn encode_record(self, name: &str) -> Result<PrettyRecordEncoder<'e, 'a>, Infallible> {
        self.inner.push("(");
        self.inner.push(name);
        let prev_indent = self.inner.set_indent(self.inner.col() + 1);
        Ok(PrettyRecordEncoder {
            enc: self,
            first: true,
            prev_indent,
        })
    }
}

pub struct PrettySequenceEncoder<'e, 'a> {
    enc: &'a mut PrettyEncoder<'e>,
    first: bool,
    prev_indent: usize,
}

impl<'e, 'a> SequenceEncoder for PrettySequenceEncoder<'e, 'a> {
    type Error = Infallible;

    fn encode_item<E: Encodable>(&mut self, item: &E) -> Result<(), Infallible> {
        if self.first {
            self.enc.inner.push(" ");
            self.first = false;
        }
        else {
            self.enc.inner.newline();
        }
        item.encode(&mut *self.enc)
    }

    fn end(self) -> Result<(), Infallible> {
        if !self.first {
            self.enc.inner.push(" ");
        }
        self.enc.inner.push("]");
        self.enc.inner.set_indent(self.prev_indent);
        Ok(())
    }
}

pub struct PrettyRecordEncoder<'e, 'a> {
    enc: &'a mut PrettyEncoder<'e>,
    first: bool,
    prev_indent: usize,
}

impl<'e, 'a> RecordEncoder for PrettyRecordEncoder<'e, 'a> {
    type Error = Infallible;

    fn encode_field<E: Encodable>(&mut self, field: &str, value: &E) -> Result<(), Infallible> {
        if self.first {
            self.enc.inner.push(" ");
            self.first = false;
        }
        else {
            self.enc.inner.newline();
        }
        self.enc.inner.push(field);
        self.enc.inner.push(" ");
        value.encode(&mut *self.enc)
    }

    fn end(self) -> Result<(), Infallible> {
        self.enc.inner.push(")");
        self.enc.inner.set_indent(self.prev_indent);
        Ok(())
    }
}
