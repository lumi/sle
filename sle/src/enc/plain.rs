use {
    core::convert::Infallible,
};

use super::{
    EncoderInner,
    Encoder,
    Encodable,
    SequenceEncoder,
    RecordEncoder,
};

pub struct PlainEncoder<'e> {
    inner: EncoderInner<'e>,
}

impl<'e> PlainEncoder<'e> {
    pub fn new(out: &'e mut String) -> PlainEncoder<'e> {
        PlainEncoder {
            inner: EncoderInner::new(out),
        }
    }
}

impl<'e, 'a> Encoder for &'a mut PlainEncoder<'e> {
    type Error = Infallible;
    type SequenceEncoder = PlainSequenceEncoder<'e, 'a>;
    type RecordEncoder = PlainRecordEncoder<'e, 'a>;

    fn encode_nil(self) -> Result<(), Infallible> {
        self.inner.push("#n");
        Ok(())
    }

    fn encode_real(self, real: f64) -> Result<(), Infallible> {
        self.inner.push(&real.to_string());
        if real.floor() == real {
            self.inner.push(".");
        }
        Ok(())
    }

    fn encode_integer(self, int: i64) -> Result<(), Infallible> {
        self.inner.push(&int.to_string());
        Ok(())
    }

    fn encode_bool(self, b: bool) -> Result<(), Infallible> {
        if b {
            self.inner.push("#t");
        }
        else {
            self.inner.push("#f");
        }
        Ok(())
    }

    fn encode_text(self, text: &str) -> Result<(), Infallible> {
        self.inner.push("\"");
        self.inner.push(&text.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\""));
        self.inner.push("\"");
        Ok(())
    }

    fn encode_atom(self, atom: &str) -> Result<(), Infallible> {
        self.inner.push(":");
        self.inner.push(atom);
        Ok(())
    }

    fn encode_sequence(self) -> Result<PlainSequenceEncoder<'e, 'a>, Infallible> {
        self.inner.push("[");
        Ok(PlainSequenceEncoder {
            enc: self,
            first: true,
        })
    }

    fn encode_record(self, name: &str) -> Result<PlainRecordEncoder<'e, 'a>, Infallible> {
        self.inner.push("(");
        self.inner.push(name);
        Ok(PlainRecordEncoder {
            enc: self,
        })
    }
}

pub struct PlainSequenceEncoder<'e, 'a> {
    enc: &'a mut PlainEncoder<'e>,
    first: bool,
}

impl<'e, 'a> SequenceEncoder for PlainSequenceEncoder<'e, 'a> {
    type Error = Infallible;

    fn encode_item<T: Encodable>(&mut self, item: &T) -> Result<(), Infallible> {
        if self.first {
            self.first = false;
        }
        else {
            self.enc.inner.push(" ");
        }
        item.encode(&mut *self.enc)
    }

    fn end(self) -> Result<(), Infallible> {
        self.enc.inner.push("]");
        Ok(())
    }
}

pub struct PlainRecordEncoder<'e, 'a> {
    enc: &'a mut PlainEncoder<'e>,
}

impl<'e, 'a> RecordEncoder for PlainRecordEncoder<'e, 'a> {
    type Error = Infallible;

    fn encode_field<E: Encodable>(&mut self, field: &str, value: &E) -> Result<(), Infallible> {
        self.enc.inner.push(" ");
        self.enc.inner.push(field);
        self.enc.inner.push(" ");
        value.encode(&mut *self.enc)
    }

    fn end(self) -> Result<(), Infallible> {
        self.enc.inner.push(")");
        Ok(())
    }
}
