//! Sub-module for everything that has to do with decoding types from SLE.

use {
    std::{
        marker::PhantomData,
    },
};

pub mod token;
pub mod impls;
mod error;

use token::{
    Token,
    TokenType,
    Tokenizer,
};

pub use error::{
    DecoderError,
};

/// This is used to parse a record, it's given to you via a visitor.
pub struct Record<'d, 'a> {
    decoder: &'a mut Decoder<'d>,
}

impl<'d, 'a> Record<'d, 'a> {
    /// Get the name of the record.
    pub fn name(&mut self) -> &str {
        self.decoder.tok.peek2().unwrap().expect_bare().unwrap()
    }

    /// Directly parse the fields, this returns something which can be used to iterate over the
    /// fields.
    pub fn direct(self) -> Result<Fields<'d, 'a>, DecoderError> {
        self.decoder.tok.next()?;
        self.decoder.tok.next()?;
        Ok(Fields {
            decoder: self.decoder,
            expecting_key: true,
            done: false,
        })
    }

    /// Delegate parsing this record to another type that implemented `Decodable`.
    pub fn delegate<D: Decodable>(self) -> Result<D, DecoderError> {
        D::decode(self.decoder)
    }
}

/// This is used to parse the fields in a record.
pub struct Fields<'d, 'a> {
    decoder: &'a mut Decoder<'d>,
    expecting_key: bool,
    done: bool,
}

impl<'d, 'a> Fields<'d, 'a> {
    /// Get the next key, if there is one.
    pub fn next_key(&mut self) -> Result<Option<&str>, DecoderError> {
        if self.done {
            panic!("contract violation: not allowed to read anything anymore");
        }
        if !self.expecting_key {
            panic!("contract violation: trying to read a key when you should be reading a value");
        }
        match self.decoder.tok.next() {
            Ok(Token::Bare(key)) => {
                self.expecting_key = false;
                Ok(Some(key))
            },
            Ok(Token::CloseRecord) => {
                self.done = true;
                Ok(None)
            },
            Ok(tok) => {
                self.done = true;
                Err(DecoderError::UnexpectedToken(tok.ty(), &[TokenType::CloseRecord, TokenType::Bare]))
            },
            Err(err) => {
                self.done = true;
                Err(err)
            },
        }
    }

    /// Get the next value, decode it using the given type, which implements `Decodable`.
    pub fn next_value<V: Decodable>(&mut self) -> Result<V, DecoderError> {
        if self.done {
            panic!("contract violation: not allowed to read anything anymore");
        }
        if self.expecting_key {
            panic!("contract violation: trying to read a value when you should be reading a key");
        }
        match V::decode(self.decoder) {
            Ok(v) => {
                self.expecting_key = true;
                Ok(v)
            },
            Err(err) => {
                self.done = true;
                Err(err)
            },
        }
    }
}

/// This is used to parse the items in a sequence.
pub struct UntypedItems<'d, 'a> {
    decoder: &'a mut Decoder<'d>,
}

impl<'d, 'a> UntypedItems<'d, 'a> {
    /// Select the type of the items in the sequence.
    pub fn select<I: Decodable>(self) -> Items<'d, 'a, I> {
        Items {
            decoder: self.decoder,
            _marker: PhantomData,
        }
    }
}

/// This is used to parse the items in a sequence.
pub struct Items<'d, 'a, I: Decodable> {
    decoder: &'a mut Decoder<'d>,
    _marker: PhantomData<I>,
}

impl<'d, 'a, I: Decodable> Items<'d, 'a, I> {
    /// Parse the next item.
    pub fn next_item(&mut self) -> Result<Option<I>, DecoderError> {
        match self.decoder.tok.peek()? {
            Token::CloseSequence => {
                self.decoder.tok.next()?;
                Ok(None)
            },
            _ => {
                Ok(Some(I::decode(&mut self.decoder)?))
            },
        }
    }
}

/// A trait that is used to turn an SLE value into a rust value.
pub trait Visitor<'d>: Sized {
    /// The returned value.
    type Value: Sized;

    /// Encountered a nil.
    fn visit_nil(self) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("nil"))
    }

    /// Encountered a bool.
    fn visit_bool(self, _bool: bool) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("bool"))
    }

    /// Encountered an integer.
    fn visit_integer(self, _integer: i64) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("integer"))
    }

    /// Encountered a real.
    fn visit_real(self, _real: f64) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("real"))
    }

    /// Encountered some text.
    fn visit_text(self, _text: &str) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("text"))
    }

    /// Encountered an atom.
    fn visit_atom(self, _atom: &str) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("atom"))
    }

    /// Encountered a record.
    fn visit_record<'a>(self, _record: Record<'d, 'a>) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("rec"))
    }

    /// Encountered a sequence.
    fn visit_sequence<'a>(self, _items: UntypedItems<'d, 'a>) -> Result<Self::Value, DecoderError> {
        Err(DecoderError::UnexpectedType("seq"))
    }
}

/// A trait for things that can be decoded from SLE.
pub trait Decodable: Sized {
    /// Decode the value using the given `Decoder`.
    fn decode<'d>(decoder: &mut Decoder<'d>) -> Result<Self, DecoderError>;

    /// Extract the value, which is present when given a `Some` and absent when given a `None`.
    ///
    /// The default implementation errors when the field doesn't exist.
    fn extract(field: &'static str, opt: Option<Self>) -> Result<Self, DecoderError> {
        match opt {
            Some(inner) => Ok(inner),
            None => Err(DecoderError::MissingField(field)),
        }
    }
}

/// Used for decoding.
pub struct Decoder<'t> {
    tok: Tokenizer<'t>,
}

impl<'t> Decoder<'t> {
    /// Create a `Decoder` from a `Tokenizer`.
    pub fn from_tokenizer(tok: Tokenizer<'t>) -> Decoder<'t> {
        Decoder { tok }
    }

    /// Decode any value.
    pub fn decode_any<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        match self.tok.peek()? {
            Token::Nil => {
                self.tok.next()?;
                visitor.visit_nil()
            },
            Token::Bool(_) => {
                let b = self.tok.next()?.expect_bool()?;
                visitor.visit_bool(b)
            },
            Token::Integer(_) => {
                let int = self.tok.next()?.expect_integer()?;
                visitor.visit_integer(int)
            },
            Token::Real(_) => {
                let real = self.tok.next()?.expect_real()?;
                visitor.visit_real(real)
            },
            Token::Text(_) => {
                let text = self.tok.next()?.expect_text()?;
                visitor.visit_text(&text)
            },
            Token::Atom(_) => {
                let atom = self.tok.next()?.expect_atom()?;
                visitor.visit_atom(atom)
            },
            Token::OpenRecord => self.decode_record(visitor),
            Token::OpenSequence => self.decode_sequence(visitor),
            tok => Err(DecoderError::UnexpectedToken(tok.ty(), &[TokenType::Nil, TokenType::Text, TokenType::Atom, TokenType::OpenRecord, TokenType::OpenSequence])),
        }
    }

    /// Decode a nil.
    pub fn decode_nil<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        let () = self.tok.next()?.expect_nil()?;
        visitor.visit_nil()
    }

    /// Decode a bool.
    pub fn decode_bool<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        let b = self.tok.next()?.expect_bool()?;
        visitor.visit_bool(b)
    }

    /// Decode an integer.
    pub fn decode_integer<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        let int = self.tok.next()?.expect_integer()?;
        visitor.visit_integer(int)
    }

    /// Decode a real.
    pub fn decode_real<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        let real = self.tok.next()?.expect_real()?;
        visitor.visit_real(real)
    }

    /// Decode some text.
    pub fn decode_text<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        let text = self.tok.next()?.expect_text()?;
        visitor.visit_text(&text)
    }

    /// Decode an atom.
    pub fn decode_atom<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        let atom = self.tok.next()?.expect_atom()?;
        visitor.visit_atom(atom)
    }

    /// Decode a record.
    pub fn decode_record<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        self.tok.peek()?.expect_open_record()?;
        self.tok.peek2()?.expect_bare()?;
        let record = Record { decoder: self };
        let inner = visitor.visit_record(record)?;
        Ok(inner)
    }

    /// Decode a sequence.
    pub fn decode_sequence<V: Visitor<'t>>(&mut self, visitor: V) -> Result<V::Value, DecoderError> {
        self.tok.next()?.expect_open_sequence()?;
        let items = UntypedItems { decoder: self };
        let inner = visitor.visit_sequence(items)?;
        Ok(inner)
    }

    /// Decode an optional type.
    pub fn decode_option<T: Decodable>(&mut self) -> Result<Option<T>, DecoderError> {
        match self.tok.peek()? {
            Token::Nil => {
                self.tok.next()?;
                Ok(None)
            },
            _ => Ok(Some(T::decode(self)?)),
        }
    }
}
