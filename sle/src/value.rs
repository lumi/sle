use {
    std::mem,
};

use super::{
    dec::{
        Visitor,
        Decoder,
        DecoderError,
        Decodable,
        Record as DecRecord,
        UntypedItems,
    },
    enc::{
        Encoder,
        Encodable,
        SequenceEncoder,
        RecordEncoder,
    },
};

#[derive(Debug, Clone, PartialEq)]
pub struct Record {
    pub name: String,
    pairs: Vec<(String, Value)>,
}

impl Record {
    pub fn new<S: Into<String>>(name: S) -> Record {
        Record {
            name: name.into(),
            pairs: Vec::new(),
        }
    }

    pub fn new_with_fields<S: Into<String>>(name: S, fields: Vec<(String, Value)>) -> Record {
        Record {
            name: name.into(),
            pairs: fields,
        }
    }

    pub fn insert<K: Into<String>, V: Into<Value>>(&mut self, key: K, value: V) -> Option<Value> {
        let actual_key = key.into();
        let actual_value = value.into();
        if let Some((_, ref mut value_slot)) = self.pairs.iter_mut().find(|(k, _)| k == &actual_key) {
            Some(mem::replace(value_slot, actual_value))
        }
        else {
            self.pairs.push((actual_key, actual_value));
            None
        }
    }

    pub fn remove<K: AsRef<str>>(&mut self, key: K) -> Option<Value> {
        let actual_key = key.as_ref();
        if let Some(idx) = self.pairs.iter().position(|(k, _)| k == actual_key) {
            Some(self.pairs.remove(idx).1)
        }
        else {
            None
        }
    }

    pub fn fields(&self) -> impl Iterator<Item = (&str, &Value)> {
        self.pairs.iter().map(|(ref k, ref v)| (k.as_ref(), v))
    }

    pub fn fields_mut(&mut self) -> impl Iterator<Item = (&str, &mut Value)> {
        self.pairs.iter_mut().map(|(ref k, ref mut v)| (k.as_ref(), v))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Nil,
    Bool(bool),
    Integer(i64),
    Real(f64),
    Text(String),
    Atom(String),
    Record(Record),
    Sequence(Vec<Value>),
}

pub struct ValueVisitor;

impl<'d> Visitor<'d> for ValueVisitor {
    type Value = Value;

    fn visit_nil(self) -> Result<Value, DecoderError> {
        Ok(Value::Nil)
    }

    fn visit_bool(self, b: bool) -> Result<Value, DecoderError> {
        Ok(Value::Bool(b))
    }

    fn visit_integer(self, int: i64) -> Result<Value, DecoderError> {
        Ok(Value::Integer(int))
    }

    fn visit_real(self, real: f64) -> Result<Value, DecoderError> {
        Ok(Value::Real(real))
    }

    fn visit_text(self, text: &str) -> Result<Value, DecoderError> {
        Ok(Value::Text(text.to_owned()))
    }

    fn visit_atom(self, atom: &str) -> Result<Value, DecoderError> {
        Ok(Value::Atom(atom.to_owned()))
    }

    fn visit_record<'a>(self, mut record: DecRecord<'d, 'a>) -> Result<Value, DecoderError> {
        let name = record.name().to_owned();
        let mut rec = Record::new(name);
        let mut fields = record.direct()?;
        while let Some(key) = fields.next_key()? {
            let key = key.to_owned();
            let val = fields.next_value::<Value>()?;
            if rec.insert(key, val).is_some() {
                return Err(DecoderError::DuplicateKey);
            }
        }
        Ok(Value::Record(rec))
    }

    fn visit_sequence<'a>(self, items: UntypedItems<'d, 'a>) -> Result<Value, DecoderError> {
        let mut typed = items.select::<Value>();
        let mut ret = Vec::new();
        while let Some(item) = typed.next_item()? {
            ret.push(item);
        }
        Ok(Value::Sequence(ret))
    }
}

impl Decodable for Value {
    fn decode<'d>(dec: &mut Decoder<'d>) -> Result<Value, DecoderError> {
        dec.decode_any(ValueVisitor)
    }
}

impl Encodable for Value {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error> {
        match *self {
            Value::Nil => enc.encode_nil(),
            Value::Bool(b) => enc.encode_bool(b),
            Value::Integer(int) => enc.encode_integer(int),
            Value::Real(real) => enc.encode_real(real),
            Value::Text(ref text) => enc.encode_text(text),
            Value::Atom(ref atom) => enc.encode_atom(atom),
            Value::Record(ref rec) => {
                let mut recenc = enc.encode_record(&rec.name)?;
                for (k, v) in rec.fields() {
                    recenc.encode_field(k, v)?;
                }
                recenc.end()
            },
            Value::Sequence(ref seq) => {
                let mut seqenc = enc.encode_sequence()?;
                for item in seq {
                    seqenc.encode_item(item)?;
                }
                seqenc.end()
            },
        }
    }
}
