//! Sub-module for everything that has to do with encoding types to SLE.

pub mod impls;
pub mod pretty;
pub mod plain;

pub use {
    pretty::PrettyEncoder,
    plain::PlainEncoder,
};

/// A trait for types that can be encoded using an implementation of `Encoder`.
pub trait Encodable {
    fn encode<E: Encoder>(&self, enc: E) -> Result<(), E::Error>;
}

/// A trait for types that can be used to encode data to SLE.
pub trait Encoder {
    type Error: std::error::Error;
    type RecordEncoder : RecordEncoder<Error = Self::Error>;
    type SequenceEncoder : SequenceEncoder<Error = Self::Error>;

    fn encode_nil(self) -> Result<(), Self::Error>;
    fn encode_bool(self, value: bool) -> Result<(), Self::Error>;
    fn encode_integer(self, value: i64) -> Result<(), Self::Error>;
    fn encode_real(self, value: f64) -> Result<(), Self::Error>;
    fn encode_text(self, value: &str) -> Result<(), Self::Error>;
    fn encode_atom(self, value: &str) -> Result<(), Self::Error>;
    fn encode_record(self, name: &str) -> Result<Self::RecordEncoder, Self::Error>;
    fn encode_sequence(self) -> Result<Self::SequenceEncoder, Self::Error>;
}

/// A trait for types that can be used to encode a sequence to SLE.
pub trait SequenceEncoder {
    type Error : std::error::Error;

    fn encode_item<E: Encodable>(&mut self, item: &E) -> Result<(), Self::Error>;
    fn end(self) -> Result<(), Self::Error>;
}

/// A trait for types that can be used to encode a record to SLE.
pub trait RecordEncoder {
    type Error : std::error::Error;

    fn encode_field<E: Encodable>(&mut self, field: &str, value: &E) -> Result<(), Self::Error>;
    fn end(self) -> Result<(), Self::Error>;
}

struct EncoderInner<'e> {
    out: &'e mut String,
    indentation: usize,
    col: usize,
}

impl<'e> EncoderInner<'e> {
    fn new(out: &'e mut String) -> EncoderInner<'e> {
        EncoderInner {
            out,
            indentation: 0,
            col: 0,
        }
    }

    fn push(&mut self, text: &str) {
        self.out.push_str(text);
        self.col += text.chars().count();
    }

    fn newline(&mut self) {
        self.out.reserve(self.indentation + 1);
        self.out.push('\n');
        for _ in 0..self.indentation {
            self.out.push(' ');
        }
        self.col = self.indentation;
    }

    fn set_indent(&mut self, indent: usize) -> usize {
        let old = self.indentation;
        self.indentation = indent;
        old
    }

    fn col(&self) -> usize {
        self.col
    }
}
