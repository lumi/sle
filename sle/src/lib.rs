//! A crate for encoding and decoding SLE.
//!
//! # Examples
//!
//! ```rust
//! use sle::{Encodable, Decodable};
//!
//! #[derive(Debug, PartialEq, Encodable, Decodable)]
//! pub struct Blog {
//!     pub title: String,
//!     pub articles: Vec<Article>,
//! }
//!
//! #[derive(Debug, PartialEq, Encodable, Decodable)]
//! pub struct Article {
//!     pub title: String,
//!     pub tags: Vec<String>,
//!     pub anger: i64,
//!     pub body: String,
//! }
//!
//! let article1 = Article {
//!     title: "ben shapiro DESTROYED with FACTS and LOGIC".to_owned(),
//!     tags: vec![ "FACTS".to_owned(), "LOGIC".to_owned() ],
//!     anger: 100,
//!     body: "rip ben shapiro".to_owned(),
//! };
//!
//! let article2 = Article {
//!     title: "hey i just met you, and this is crazy".to_owned(),
//!     tags: vec![ "why".to_owned(), "music".to_owned(), "flirting".to_owned() ],
//!     anger: -10,
//!     body: "so here's my jid, xmpp me maybe?".to_owned(),
//! };
//!
//! let article3 = Article {
//!     title: "copyright considered harmful".to_owned(),
//!     tags: vec![ "capitalism".to_owned(), "finally".to_owned() ],
//!     anger: 1000,
//!     body: "can we just stop with IP law in general? thanks".to_owned(),
//! };
//!
//! let blog = Blog {
//!     title: "opinions 2.0".to_owned(),
//!     articles: vec![ article1, article2, article3 ],
//! };
//!
//! let mut out = String::new();
//! sle::encode(&blog, &mut out);
//! let new_blog: Blog = sle::decode(&out).unwrap();
//! assert_eq!(blog, new_blog);
//! ```

pub mod dec;
pub mod enc;
pub mod value;

#[cfg(test)] mod tests;

pub use {
    value::{Value, Record},
    dec::Decodable,
    enc::Encodable,
    sle_derive::{
        Encodable,
        Decodable,
    },
};

use crate::{
    dec::{
        DecoderError,
        token::Tokenizer,
        Decoder,
    },
    enc::{
        PlainEncoder,
        PrettyEncoder,
    },
};

/// Decode a `Decodable` tyoe,
///
/// # Examples
///
/// ```rust
/// # use sle::Decodable;
/// #[derive(Decodable, PartialEq, Debug)]
/// struct Cat {
///     name: String,
///     behavior: Behavior,
///     age: i64,
/// }
///
/// #[derive(Decodable, PartialEq, Debug)]
/// enum Behavior {
///     Good,
///     Naughty,
///     Incomprehensible,
/// }
///
/// let kitty: Cat = sle::decode("(cat name \"kitty\" behavior :good age 4)").unwrap();
/// let skittles: Cat = sle::decode("(cat name \"skittles\" behavior :naughty age 1)").unwrap();
/// let sparky: Cat = sle::decode("(cat name \"sparky\" behavior :incomprehensible age 1)").unwrap();
///
/// assert_eq!(kitty, Cat {
///     name: "kitty".to_owned(),
///     behavior: Behavior::Good,
///     age: 4,
/// });
/// assert_eq!(skittles, Cat {
///     name: "skittles".to_owned(),
///     behavior: Behavior::Naughty,
///     age: 1,
/// });
/// assert_eq!(sparky, Cat {
///     name: "sparky".to_owned(),
///     behavior: Behavior::Incomprehensible,
///     age: 1,
/// });
/// ```
pub fn decode<D: Decodable>(text: &str) -> Result<D, DecoderError> {
    let tok = Tokenizer::from_str(text);
    let mut dec = Decoder::from_tokenizer(tok);
    D::decode(&mut dec)
}

/// Encode an `Encodable` type in short form.
///
/// # Examples
///
/// ```rust
/// # use sle::Encodable;
/// #[derive(Encodable)]
/// struct Cow {
///     name: String,
///     moos: u64,
/// }
///
/// let mut out = String::new();
///
/// sle::encode(&Cow {
///     name: "bella".to_owned(),
///     moos: 500,
/// }, &mut out);
/// assert_eq!(out, "(cow name \"bella\" moos 500)");
///
/// out.clear();
///
/// sle::encode(&Cow {
///     name: "sparky".to_owned(),
///     moos: 120_000,
/// }, &mut out);
/// assert_eq!(out, "(cow name \"sparky\" moos 120000)");
/// ```
pub fn encode<E: Encodable>(ent: &E, out: &mut String) {
    let mut enc = PlainEncoder::new(out);
    if let Err(_) = ent.encode(&mut enc) {
        unreachable!()
    }
}

/// Encode an `Encodable` type in long, pretty-printed form.
pub fn encode_pretty<E: Encodable>(ent: &E, out: &mut String) {
    let mut enc = PrettyEncoder::new(out);
    if let Err(_) = ent.encode(&mut enc) {
        unreachable!()
    }
}
