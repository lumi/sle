use {
    sle::{
        Encodable,
        Decodable,
    },
};

#[derive(Debug, Encodable, Decodable)]
pub struct Blog {
    pub title: String,
    pub articles: Vec<Article>,
}

#[derive(Debug, Encodable, Decodable)]
pub struct Article {
    pub title: String,
    pub tags: Vec<String>,
    pub anger: i64,
    pub body: String,
}

fn main() {
    let frame: Blog = sle::decode(r#"
        (blog title "some blog"
              articles [ (article title "thing bad, very bad"
                                  tags [ "thing" "bad" "angry" ]
                                  anger 10
                                  body "you don't want to know" )
                         (article title "cake is good"
                                  tags [ "food" "cake" "ohmygod" ]
                                  anger 0
                                  body "yes <3" )
                         (article title "big gay sighted near forest"
                                  tags [ "gay" "gay" "gay" ]
                                  anger -5
                                  body "so gay" )])
    "#).unwrap();
    let mut out = String::new();
    sle::encode_pretty(&frame, &mut out);
    println!("{}", out);
}
