module Text.Sle.Util
  ( bareStartChar
  , bareChar
  , atomChar
  ) where

bareStartChar, bareChar, atomChar :: [Char]
bareStartChar = "abcdefghijklmnopqrstuvwxyz"
bareChar = "abcdefghijklmnopqrstuvwxyz0123456789/-"
atomChar = "abcdefghijklmnopqrstuvwxyz0123456789/-"
