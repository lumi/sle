module Text.Sle.Encoder (toString) where

import Text.Sle.Value (Value (TextVal, AtomVal, NilVal, BoolVal, IntVal, RealVal, RecordVal, SequenceVal))

import Numeric (showFFloat)

import qualified Data.Map

import Data.List (intercalate)

toString :: Value -> String
toString (TextVal s)      = "\"" ++ applyEscapes s ++ "\"" -- TODO: escapes
toString (AtomVal a)      = ":" ++ a
toString NilVal           = "#n"
toString (BoolVal True)   = "#t"
toString (BoolVal False)  = "#f"
toString (IntVal i)       = show i
toString (RealVal r)      = showFFloat Nothing r ""
toString (RecordVal n fs) = "(" ++ n ++ concatMap fieldToString (Data.Map.toList fs) ++ ")"
toString (SequenceVal is) = "[" ++ intercalate " " (map toString is) ++ "]"

fieldToString :: (String, Value) -> String
fieldToString (f, v) = " " ++ f ++ " " ++ toString v

applyEscapes :: String -> String
applyEscapes ('\n' : cs) = '\\' : '\n' : applyEscapes cs
applyEscapes ('"'  : cs) = '\\' : '"'  : applyEscapes cs
applyEscapes ('\\' : cs) = '\\' : '\\' : applyEscapes cs
applyEscapes (c    : cs) = c           : applyEscapes cs
applyEscapes ""          = ""
