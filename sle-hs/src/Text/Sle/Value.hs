module Text.Sle.Value
  ( Value ( NilVal
          , BoolVal
          , IntVal
          , RealVal
          , TextVal
          , AtomVal
          , RecordVal
          , SequenceVal )
  , isValid
  , isValidBare
  ) where

import qualified Data.Map as M
import Data.List (elem)

import Text.Sle.Util (bareStartChar, bareChar, atomChar)

data Value
  = NilVal
  | BoolVal Bool
  | IntVal Int
  | RealVal Double
  | TextVal String
  | AtomVal String
  | RecordVal String (M.Map String Value)
  | SequenceVal [Value]
  deriving (Show, Eq)

isValidBare :: String -> Bool
isValidBare "" = False
isValidBare (s:ss) = s `elem` bareStartChar && all (`elem` bareChar) ss

isValid :: Value -> Bool
isValid (TextVal text) = if null text then True else last text /= '\\'
isValid (AtomVal atom) = all (`elem` atomChar) atom
isValid (RecordVal name fields) = isValidBare name && all (\(k, v) -> isValidBare k && isValid v) (M.toList fields)
isValid (SequenceVal items) = all isValid items
isValid _ = True
