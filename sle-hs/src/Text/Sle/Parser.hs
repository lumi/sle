module Text.Sle.Parser
  ( bareStartChar
  , bareChar
  , atomChar
  , stringParser
  , atomParser
  , nilParser
  , boolParser
  , intParser
  , realParser
  , recordParser
  , sequenceParser
  , valueParser
  , parseValue ) where

import Text.Sle.Value (Value (TextVal, AtomVal, NilVal, BoolVal, IntVal, RealVal, RecordVal, SequenceVal))
import Text.Sle.Util (bareStartChar, bareChar, atomChar)

import Control.Monad (void)
import Control.Applicative ((<|>), many)

import qualified Data.Map

import Text.Parsec (ParseError, parse)
import Text.Parsec.String (Parser)
import Text.Parsec.Prim (try)
import Text.Parsec.Char (oneOf, noneOf, char, anyChar, digit, string)
import Text.Parsec.Combinator (eof, many1, option)

whitespace :: Parser ()
whitespace = void $ many (oneOf " \n")

lexeme :: Parser a -> Parser a
lexeme p = whitespace *> p

bare :: Parser String
bare = do first <- oneOf bareStartChar
          other <- many (oneOf bareChar)
          return $ first : other

stringParser :: Parser Value
stringParser = do _ <- char '"'
                  inner <- many (escapedParser <|> noneOf "\"")
                  _ <- char '"'
                  return $ TextVal inner
  where escapedParser :: Parser Char
        escapedParser = do
          _ <- char '\\'
          anyChar >>= \case
            '\\' -> return '\\'
            '"'  -> return '"'
            'n'  -> return '\n'
            c    -> return c

atomParser :: Parser Value
atomParser = do _ <- char ':'
                inner <- many (oneOf atomChar)
                return $ AtomVal inner

nilParser :: Parser Value
nilParser = string "#n" >> return NilVal

boolParser :: Parser Value
boolParser = fmap BoolVal (char '#' >> (trueParser <|> falseParser))
  where trueParser, falseParser :: Parser Bool
        trueParser = char 't' >> return True
        falseParser = char 'f' >> return False

intParser :: Parser Value
intParser = IntVal . read <$> do neg <- option "" $ string "-"
                                 digits <- many1 digit
                                 return (neg ++ digits)

realParser :: Parser Value
realParser = RealVal . read . concat <$> sequence [option "" (string "-"), many1 digit, string ".", many1 digit]

recordParser :: Parser Value
recordParser = do _ <- lexeme $ char '('
                  name <- lexeme bare
                  fields <- Data.Map.fromList <$> many fieldParser
                  _ <- lexeme $ char ')'
                  return $ RecordVal name fields
  where fieldParser :: Parser (String, Value)
        fieldParser = do name <- lexeme bare
                         value <- lexeme valueParser
                         return $ (name, value)

sequenceParser :: Parser Value
sequenceParser = SequenceVal <$> ((lexeme $ char '[') *> many valueParser <* (lexeme $ char ']'))

valueParser :: Parser Value
valueParser = lexeme $ stringParser
                   <|> atomParser
                   <|> try nilParser
                   <|> try boolParser
                   <|> try realParser
                   <|> intParser
          <|> recordParser
          <|> sequenceParser

parseValue :: String -> Either ParseError Value
parseValue = parse (valueParser <* whitespace <* eof) ""
