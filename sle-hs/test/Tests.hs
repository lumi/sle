module Tests where

import Distribution.TestSuite.QuickCheck

import Test.QuickCheck ((==>), Property)
import Test.QuickCheck.Gen (Gen, oneof, listOf, listOf1, elements, frequency)
import Test.QuickCheck.Arbitrary (Arbitrary, arbitrary, shrink)

import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.Char as C

import qualified Text.Sle.Encoder as E
import qualified Text.Sle.Parser as P
import qualified Text.Sle.Value as V
import qualified Text.Sle.Util as U

tests :: IO [Test]
tests = return [ testProperty "arbitrary Values are valid" prop_arbValueWorks
               , testProperty "Encode-Decode" prop_encodeDecode
               ]

arbAtomString :: Gen String
arbAtomString = listOf1 (elements U.atomChar)

arbBareString :: Gen String
arbBareString = (:) <$> elements U.bareStartChar <*> listOf (elements U.bareChar)

shrinkNonEmptyString :: String -> [String]
shrinkNonEmptyString = filter (not . L.null) . shrink

shrinkBareString :: String -> [String]
shrinkBareString = map (map C.toLower) . filter ((`elem` U.bareStartChar) . head) . shrinkNonEmptyString

shrinkFields :: M.Map String V.Value -> [M.Map String V.Value]
shrinkFields = take 10 . filter (\fs -> all V.isValidBare (M.keys fs)) . shrink

arbitraryFields :: Gen (M.Map String V.Value)
arbitraryFields = M.fromList <$> listOf field
  where field :: Gen (String, V.Value)
        field = do name <- arbBareString
                   val <- arbitrary
                   return (name, val)

instance Arbitrary V.Value where
  arbitrary = frequency [ (10 , return V.NilVal)
                        , (10 , V.BoolVal <$> arbitrary)
                        , (30 , V.IntVal <$> arbitrary)
                        , (30 , V.RealVal <$> arbitrary)
                        , (50 , V.TextVal <$> arbitrary)
                        , (50 , V.AtomVal <$> arbAtomString)
                        , (2  , V.RecordVal <$> arbBareString <*> arbitraryFields)
                        , (2  , V.SequenceVal <$> arbitrary)
                        ]

  shrink (V.TextVal text) = V.TextVal <$> shrink text
  shrink (V.AtomVal atom) = V.AtomVal <$> shrinkNonEmptyString atom
  shrink (V.RecordVal name fields) = V.RecordVal <$> shrinkBareString name <*> shrinkFields fields
  shrink (V.SequenceVal items) = V.SequenceVal <$> shrink items
  shrink _ = []

prop_arbValueWorks :: V.Value -> Bool
prop_arbValueWorks val = V.isValid val

prop_encodeDecode :: V.Value -> Property
prop_encodeDecode val = V.isValid val ==> P.parseValue (E.toString val) == Right val
