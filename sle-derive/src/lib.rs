extern crate proc_macro;

use {
    quote::{
        quote,
    },
    syn::{
        parse_macro_input,
        DeriveInput,
    },
};

mod enc;
mod dec;
mod util;

#[proc_macro_derive(Encodable)]
pub fn encodable_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let generics = enc::add_encodable_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let full_impl = enc::encodable_impl_fn(&name, &input.data);

    let expanded = quote! {
        impl #impl_generics ::sle::enc::Encodable for #name #ty_generics #where_clause {
            #full_impl
        }
    };

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(Decodable)]
pub fn decodable_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let generics = dec::add_decodable_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let full_impl = dec::decodable_impl_fn(&name, &input.data);

    let expanded = quote! {
        impl #impl_generics ::sle::dec::Decodable for #name #ty_generics #where_clause {
            #full_impl
        }
    };

    proc_macro::TokenStream::from(expanded)
}
