use {
    proc_macro2::TokenStream,
    quote::{
        quote,
        quote_spanned,
    },
    syn::{
        Generics,
        GenericParam,
        Path,
        Data,
        Fields,
        Ident,
        DataStruct,
        DataEnum,
        FieldsNamed,
        parse_quote,
        spanned::Spanned,
    },
};

use crate::{
    util::to_dash_case,
};

pub fn add_decodable_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(::sle::dec::Decodable));
        }
    }
    generics
}

pub fn decodable_impl_fn(name: &Ident, data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => struct_decodable_impl_fn(name, data),
        Data::Enum(ref data) => enum_decodable_impl_fn(name, data),
        Data::Union(_) => panic!("unions are not supported"),
    }
}

fn field_decoders(ctor: &Path, name: &Ident, recname: &str, fields: &FieldsNamed) -> TokenStream {
    let field_init = fields.named.iter().map(|f| {
        let fname = &f.ident;
        let ftype = &f.ty;
        quote_spanned!(f.span() => let mut #fname : Option<#ftype> = None;)
    });
    let field_assign = fields.named.iter().map(|f| {
        let fname = &f.ident;
        let fkey = to_dash_case(&fname.as_ref().unwrap().to_string());
        let ftype = &f.ty;
        quote_spanned!(f.span() => #fkey => {
            if #fname.is_some() {
                return Err(::sle::dec::DecoderError::DuplicateKey);
            }
            #fname = Some(__sle_record_fields.next_value::< #ftype >()?);
        })
    });
    let field_extractor = fields.named.iter().map(|f| {
        let fname = &f.ident;
        let ftype = &f.ty;
        let fkey = to_dash_case(&fname.as_ref().unwrap().to_string());
        quote_spanned!(f.span() => let #fname = <#ftype as ::sle::dec::Decodable>::extract(#fkey, #fname)?;)
    });
    let field_spread = fields.named.iter().map(|f| {
        let fname = &f.ident;
        quote_spanned!(f.span() => #fname ,)
    });
    quote! {
        struct __Sle_Visitor;
        impl<'d> ::sle::dec::Visitor<'d> for __Sle_Visitor {
            type Value = #name;

            fn visit_record<'a>(self, mut __sle_record: ::sle::dec::Record<'d, 'a>) -> Result<#name, ::sle::dec::DecoderError> {
                let __sle_record_name = __sle_record.name();
                if __sle_record_name != #recname {
                    return Err(::sle::dec::DecoderError::UnexpectedRecordName(__sle_record_name.to_string(), &[#recname]));
                }
                let mut __sle_record_fields = __sle_record.direct()?;
                #(#field_init)*
                while let Some(__sle_field_key) = __sle_record_fields.next_key()? {
                    match __sle_field_key {
                        #(#field_assign ,)*
                        _ => {
                            return Err(::sle::dec::DecoderError::UnexpectedField(__sle_field_key.to_owned()));
                        },
                    }
                }
                #(#field_extractor)*
                Ok(#ctor {
                    #(#field_spread)*
                })
            }
        }
        __sle_decoder.decode_record(__Sle_Visitor)
    }
}

fn struct_decodable_impl_fn(name: &Ident, data: &DataStruct) -> TokenStream {
    let inner = match data.fields {
        Fields::Named(ref fields) => {
            let recname = to_dash_case(&name.to_string());
            let ctor: Path = parse_quote!(#name);
            field_decoders(&ctor, name, &recname, fields)
        },
        Fields::Unnamed(ref _fields) => unimplemented!(),
        Fields::Unit => unimplemented!(),
    };
    quote! {
        fn decode<'d>(__sle_decoder: &mut ::sle::dec::Decoder<'d>) -> Result<#name, ::sle::dec::DecoderError> {
            #inner
        }
    }
}

fn enum_decodable_impl_fn(name: &Ident, data: &DataEnum) -> TokenStream {
    let use_atoms = data.variants.iter().all(|v| if let Fields::Unit = v.fields {
        true
    } else {
        false
    });
    let use_records = !data.variants.iter().any(|v| if let Fields::Unit = v.fields {
        true
    } else {
        false
    });
    if use_atoms == use_records {
        panic!("can't mix atoms and records in an enum");
    }
    if use_atoms {
        let variant_fields = data.variants.iter().map(|v| {
            let vname = &v.ident;
            let vkey = to_dash_case(&vname.to_string());
            match v.fields {
                Fields::Unit => {
                    quote_spanned!(v.span() => #vkey => Ok( #name :: #vname ))
                },
                _ => unreachable!(),
            }
        });
        let variant_names = data.variants.iter().map(|v| format!(":{}", to_dash_case(&v.ident.to_string())));
        quote! {
            fn decode<'d>(__sle_decoder: &mut ::sle::dec::Decoder<'d>) -> Result<#name, ::sle::dec::DecoderError> {
                struct _Sle_Visitor;
                impl<'d> ::sle::dec::Visitor<'d> for _Sle_Visitor {
                    type Value = #name;

                    fn visit_atom(self, __sle_atom: &str) -> Result<#name, ::sle::dec::DecoderError> {
                        match __sle_atom {
                            #(#variant_fields ,)*
                            _ => Err(::sle::dec::DecoderError::UnexpectedAtom(__sle_atom.to_string(), &[#(#variant_names),*])),
                        }
                    }
                }
                __sle_decoder.decode_atom(_Sle_Visitor)
            }
        }
    }
    else {
        let variant_fields = data.variants.iter().map(|v| {
            let vname = &v.ident;
            let vkey = to_dash_case(&vname.to_string());
            match v.fields {
                Fields::Named(ref fields) => {
                    let ctor: Path = parse_quote!(#name :: #vname);
                    let field_init = fields.named.iter().map(|f| {
                        let fname = &f.ident;
                        let ftype = &f.ty;
                        quote_spanned!(f.span() => let mut #fname : Option<#ftype> = None;)
                    });
                    let field_assign = fields.named.iter().map(|f| {
                        let fname = &f.ident;
                        let fkey = to_dash_case(&fname.as_ref().unwrap().to_string());
                        let ftype = &f.ty;
                        quote_spanned!(f.span() => #fkey => {
                            if #fname.is_some() {
                                return Err(::sle::dec::DecoderError::DuplicateKey);
                            }
                            #fname = Some(__sle_record_fields.next_value::< #ftype >()?);
                        })
                    });
                    let field_extractor = fields.named.iter().map(|f| {
                        let fname = &f.ident;
                        let ftype = &f.ty;
                        let fkey = to_dash_case(&fname.as_ref().unwrap().to_string());
                        quote_spanned!(f.span() => let #fname = <#ftype as ::sle::dec::Decodable>::extract(#fkey, #fname)?;)
                    });
                    let field_spread = fields.named.iter().map(|f| {
                        let fname = &f.ident;
                        quote_spanned!(f.span() => #fname ,)
                    });
                    quote_spanned!(v.span() => #vkey => {
                        let mut __sle_record_fields = __sle_record.direct()?;
                        #(#field_init)*
                        while let Some(__sle_field_key) = __sle_record_fields.next_key()? {
                            match __sle_field_key {
                                #(#field_assign ,)*
                                _ => {
                                    return Err(::sle::dec::DecoderError::UnexpectedField(__sle_field_key.to_owned()));
                                },
                            }
                        }
                        #(#field_extractor)*
                        Ok(#ctor {
                            #(#field_spread)*
                        })
                    })
                },
                Fields::Unnamed(ref fields) => {
                    if fields.unnamed.len() != 1 {
                        panic!("unnamed variants can only support one field");
                    }
                    let ftype = &fields.unnamed.iter().next().unwrap().ty;
                    quote_spanned!(v.span() => #vkey => {
                        Ok( #name :: #vname ( __sle_record.delegate::<#ftype>()? ) )
                    })
                },
                Fields::Unit => unreachable!(),
            }
        });
        let variant_names = data.variants.iter().map(|v| to_dash_case(&v.ident.to_string()));
        quote! {
            fn decode<'d>(__sle_decoder: &mut ::sle::dec::Decoder<'d>) -> Result<#name, ::sle::dec::DecoderError> {
                struct _Sle_Visitor;
                impl<'d> ::sle::dec::Visitor<'d> for _Sle_Visitor {
                    type Value = #name;

                    fn visit_record<'a>(self, mut __sle_record: ::sle::dec::Record<'d, 'a>) -> Result<#name, ::sle::dec::DecoderError> {
                        let __sle_record_name = __sle_record.name();
                        match __sle_record_name {
                            #(#variant_fields,)*
                            _ => {
                                return Err(::sle::dec::DecoderError::UnexpectedRecordName(__sle_record_name.to_owned(), &[#(#variant_names),*]));
                            },
                        }
                    }
                }
                __sle_decoder.decode_record(_Sle_Visitor)
            }
        }
    }
}
