use {
    proc_macro2::TokenStream,
    quote::{
        quote,
        quote_spanned,
    },
    syn::{
        Generics,
        GenericParam,
        Data,
        FieldsNamed,
        Fields,
        Ident,
        DataEnum,
        DataStruct,
        parse_quote,
        spanned::Spanned,
    },
};

use crate::{
    util::to_dash_case,
};

pub fn add_encodable_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(::sle::enc::Encodable));
        }
    }
    generics
}

pub fn encodable_impl_fn(name: &Ident, data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => struct_encodable_impl_fn(name, data),
        Data::Enum(ref data) => enum_encodable_impl_fn(name, data),
        Data::Union(_) => panic!("unions are not supported"),
    }
}

fn field_encoders(recname: &str, fields: &FieldsNamed) -> TokenStream {
    let field_matches = fields.named.iter().map(|f| {
        let name = &f.ident;
        quote_spanned!(f.span() => ref #name)
    });
    let field_encoders = fields.named.iter().map(|f| {
        let name = &f.ident;
        let field_name = to_dash_case(&name.as_ref().unwrap().to_string());
        quote_spanned!(f.span() => __sle_rec_encoder.encode_field(#field_name, #name)?;)
    });
    quote! {
        { #( #field_matches ),* } => {
            let mut __sle_rec_encoder = __sle_encoder.encode_record(#recname)?;
            #( #field_encoders )*
            __sle_rec_encoder.end()
        },
    }
}

fn struct_encodable_impl_fn(name: &Ident, data: &DataStruct) -> TokenStream {
    let inner = match data.fields {
        Fields::Named(ref fields) => {
            let recname = to_dash_case(&name.to_string());
            let inner = field_encoders(&recname, fields);
            quote_spanned!(fields.span() => match *self {
                #name #inner
            })
        },
        Fields::Unnamed(ref fields) => {
            if fields.unnamed.len() != 1 {
                panic!("only newtype structs are supported");
            }
            quote_spanned!(fields.span() => self.0.encode(__sle_encoder))
        },
        Fields::Unit => {
            panic!("unit structs can't be encoded");
        },
    };
    quote! {
        fn encode<'e, E: ::sle::enc::Encoder>(&self, __sle_encoder: E) -> Result<(), E::Error> {
            use ::sle::enc::RecordEncoder;
            #inner
        }
    }
}

fn enum_encodable_impl_fn(name: &Ident, data: &DataEnum) -> TokenStream {
    let variant_impls = data.variants.iter().map(|v| {
        let vname = &v.ident;
        let var_name = to_dash_case(&v.ident.to_string());
        match v.fields {
            Fields::Named(ref fields) => {
                let inner = field_encoders(&var_name, fields);
                quote_spanned!(v.span() => #name :: #vname #inner)
            },
            Fields::Unnamed(ref fields) => {
                if fields.unnamed.len() != 1 {
                    panic!("unnamed variants can only support one field");
                }
                quote_spanned!(v.span() => #name :: #vname ( ref inner ) => {
                    inner.encode(__sle_encoder)
                })
            },
            Fields::Unit => {
                quote_spanned!(v.span() => #name :: #vname => {
                    __sle_encoder.encode_atom(#var_name)
                })
            },
        }
    });
    quote! {
        fn encode<'a, E: ::sle::enc::Encoder>(&self, __sle_encoder: E) -> Result<(), E::Error> {
            use ::sle::enc::RecordEncoder;
            match *self {
                #(#variant_impls)*
            }
        }
    }
}
