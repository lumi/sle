pub fn to_dash_case(s: &str) -> String {
    if s.is_empty() { return String::new(); }
    let caps: usize = s.chars().skip(1).map(|c| if c.is_uppercase() { 1 } else { 0 }).sum();
    let mut out = String::with_capacity(s.len() + caps);
    let mut cs = s.chars();
    let first = cs.next().unwrap();
    out.extend(first.to_lowercase());
    for c in cs {
        if c.is_uppercase() {
            out.push('-');
        }
        if c == '_' {
            continue;
        }
        out.extend(c.to_lowercase());
    }
    out
}
